import React from 'react';
import { Form, Formik,Field } from 'formik';


const handleValidate = ({ description,due,time }) => {
  const errors = {};
  if (!description) errors.description = "Required";
  if (!due) errors.due = "Required";
  if (!time) errors.time = "Required";
  
  return errors;
}

const handleSubmit = (props) => async (values, {setSubmitting, resetForm}) => {
    await props.onTodoCreation({...values});   
    setSubmitting(false);
    resetForm();
};

const TodoForm = (props) => {
  return (
    <div>
      <h2>Todo Form</h2>
        <Formik
          enableReinitialize={true}
          initialValues={{ 
                description: '', 
                due: new Date().toISOString().substring(0, 10),
                time: '00:00',
                priority: 'NORMAL'
          }}
          validate={handleValidate}
          onSubmit={handleSubmit(props)}
      >
          {
              props => (
                <Form>
                    <label htmlFor="description">Description</label>
                    <div>
                        <input
                            id="description"
                            name="description"
                            type="description"
                            placeholder="I need to..."
                            value={props.values.description}
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            style={{
                                borderColor:
                                    props.errors.description && props.touched.description && "red"
                            }}
                        />
                        {props.errors.description && props.touched.description && (
                            <div style={{ color: "red" }}>{props.errors.description}</div>
                        )}
                    </div>
                    
                    <label htmlFor="due">Due</label>
                    <div>
                        <input
                            id="due"
                            name="due"
                            type="date"
                            placeholder="I need to..."
                            value={props.values.due}
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            style={{
                                borderColor:
                                    props.errors.due && props.touched.due && "red"
                            }}
                            
                        />
                        {props.errors.due && props.touched.due && (
                            <div style={{ color: "red" }}>{props.errors.due}</div>
                        )}
                    </div>
                    
                    <label htmlFor="time">Time</label>
                    <div>
                        <input
                            id="time"
                            name="time"
                            type="time"
                            value={props.values.time}
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            style={{
                                borderColor:
                                    props.errors.time && props.touched.time && "red"
                            }}
                            
                        />
                        {props.errors.time && props.touched.time && (
                            <div style={{ color: "red" }}>{props.errors.time}</div>
                        )}
                    </div>
                    
                    <label htmlFor="priority">Priority</label>
                    <div>
                        <Field 
                            id="priority" 
                            name="priority" 
                            component="select" 
                        >
                          <option value="NORMAL">NORMAL</option>
                          <option value="MEDIUM">MEDIUM</option>
                          <option value="HIGH">HIGH</option>
                        </Field>
                    </div>
                    
                    <input
                        type="submit"
                        value="Submit"
                        disabled={props.isSubmitting}
                    />
                    <input
                        type="reset"
                        value="Reset"
                        onClick={props.handleReset}
                        disabled={!props.dirty || props.isSubmitting}
                    />
                </Form>
            )
          }
      </Formik>
    </div>
  );
};

export default TodoForm;
