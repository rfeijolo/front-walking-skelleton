import React, { Fragment } from 'react';
import './TodoList.css'

const todoListTableHeader = () => {
  return (
    <Fragment>
      <th>
        Priority   
      </th>
      <th>
        Description   
      </th>
      <th>
        Due   
      </th>
      <th>
        Status   
      </th>
      <th>
        Act   
      </th>
      <th>
        Del   
      </th>
    </Fragment>
  );
};

const todoListItem = props => (
  props.todos.map(todo => (
    <tr key={todo.uuid}>
      <td>  
        {todo.priority}
      </td>
      <td>  
        {todo.description}
      </td>
      <td>  
        {''+((new Date(todo.due)).getDate()+1)+'/'+((new Date(todo.due)).getMonth()+1)+' - '+(todo.due).substring(11,16)}
      </td>
      <td>  
        {todo.status}
      </td>
      <td>
        <button
          disabled= {todo.status === 'COMPLETED' ? true : false}
          onClick={todo.status === 'WAITING' ? () => props.onTodoStart(todo.uuid) : () => props.onTodoEnd(todo.uuid)} 
        >
          V
        </button>
      </td> 
      <td>
        <button 
          onClick={() => props.onTodoDelete(todo.uuid)} 
        >
          X
        </button>
      </td>  
    </tr>
  ))
);


const TodoList = (props) => {
  return (
      <div>
        <h2>Todos List</h2>
        {!props.loadingList ? 
          props.todos.length > 0 ? 
            <table 
              border='1' 
              frame='hsides' 
              rules='rows' 
              cellSpacing="5" 
              cellPadding="5" 
              width="80%" 
            >
                {todoListTableHeader()}
                {todoListItem(props)}
            </table> 
            : 'EMPTY LIST' 
          : 'LOADING'}
      </div>
  );
};                                                         

export default TodoList;
