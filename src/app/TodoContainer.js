import React, { Component, Fragment } from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import Footer from './Footer';
import Header from './Header';

class TodoContainer extends Component {

  state = {
    todos: [],
    loadingList: true,
    
  }

  componentDidMount() {
    
    this.getTodosList();
    
  }

  handleTodoCreation = async (todo) => {
    const rawResponse = await fetch('https://back-walking-skelleton.herokuapp.com/todos', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          description: todo.description,
          due: todo.due+'T'+todo.time, 
          priority: todo.priority,
          status: 'WAITING'
      })
    });
    const content = await rawResponse.json();
    
    alert(content.description+' | '+content.uuid)
  
    await this.getTodosList()
  }
  
  
  
  handleTodoEnd = (uuid) => {
      
      (async () => {
        
        const rawResponse = await fetch('https://back-walking-skelleton.herokuapp.com/todos/'+uuid, {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({end: new Date(),status: 'COMPLETED'})
        });
        
        const content = await rawResponse.json();
        
        console.log(content)
        
        await this.getTodosList()
        
      })();
      
  }
  
  handleTodoStart = (uuid) => {
      
      (async () => {
        
        const rawResponse = await fetch('https://back-walking-skelleton.herokuapp.com/todos/'+uuid, {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({start: new Date(),status: 'PENDING'})
        });
        
        const content = await rawResponse.json();
        
        console.log(content)
        
        await this.getTodosList()
        
      })();
      
  }
  
  handleTodoDelete = (uuid) => {
    
      (async () => {
        await fetch('https://back-walking-skelleton.herokuapp.com/todos/'+uuid, {
          method: 'DELETE',
        });
        await this.getTodosList()
      })();
      
  }

  getTodosList(){
      
      fetch("https://back-walking-skelleton.herokuapp.com/todos")
        .then(response => response.json())
        .then(data =>
            this.setState(prevState => ({
                todos: data,
                loadingList: false,
            }))
        )
      
  }

  render() {
    return (
      <Fragment>
        <Header />
        <center>
          <TodoForm
            onTodoCreation={this.handleTodoCreation}
          />
          <TodoList
            todos={this.state.todos}
            loadingList={this.state.loadingList}
            onTodoDelete={this.handleTodoDelete}
            onTodoEnd={this.handleTodoEnd}
            onTodoStart={this.handleTodoStart}
          />
        </center>
        <Footer />
      </Fragment>
    );
  }
};

export default TodoContainer;
